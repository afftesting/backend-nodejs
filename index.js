//file: index.js

require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const request = require('request-promise');

let mongoose = require('mongoose');

//connect mongodb
mongoose.connect(process.env.MONGODB_URL);
mongoose.Promise = global.Promise;

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(require('./routes'));

const port = process.env.PORT || 3456;

app.listen(port, (err) => {
    if (err) {
        return console.error(err);
    }

    console.log("RUNNING ON PORT ", port);
});

