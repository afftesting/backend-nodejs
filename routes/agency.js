//file: afftesting.js

//path: /afftesting
const AgencyController = require('../app/controllers/AgencyController');

const router = require('express').Router();

router.use(require('../app/middlewares/APIKeyMiddleware'));

router.get('/info', AgencyController.getAgencyInfo);
router.post('/', AgencyController.storeAgency)

module.exports = router;
