//file: index.js

const router = require('express').Router();

router.use(require('../app/middlewares/CorsMiddleware'));

router.use("/afftesting", require('./afftesting'));
router.use("/agency", require('./agency'));

module.exports = router;

