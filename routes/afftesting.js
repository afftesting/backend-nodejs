//file: afftesting.js

//path: /afftesting
const AffController = require('../app/controllers/AffController');

const router = require('express').Router();

router.use(require('../app/middlewares/APIKeyMiddleware'));

router.post('/', AffController.testing);

module.exports = router;
