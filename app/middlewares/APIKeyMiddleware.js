//file: CorsMiddleware.js

const router = require('express').Router();

router.use((req, res, next) => {

    if (req.method === 'OPTIONS') {
        next();
        return;
    }

    let api_key = req.headers.authorization;

    //TODO: validate api_key in database
    if (api_key && ['adminadmin', 'adminadminadmin'].includes(api_key)) {
        next();
        return;
    }

    res.status(401).send('unauthorized');
});

module.exports = router;

