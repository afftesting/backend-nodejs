//file: LogHelper.js

const RedirectLog = require('../models/RedirectLog');

module.exports = {
    async redirectLog({from_ip, target_url, to_url, status_code, user_agent}) {
        try {
            let log = new RedirectLog({from_ip, target_url, to_url, status_code, user_agent});
            log.save();
        } catch (e) {

        }
    }
};