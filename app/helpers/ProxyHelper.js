//file: ProxyHelper.js

const request = require('request-promise');
const cheerio = require('cheerio');

const listCountry = require('../../storage/countries');

module.exports = {
    async getProxy({country = 'vn', limit = 2}) {

        if (!listCountry[country]) {
            country = 'vn'
        }

        const url = `http://www.xroxy.com/proxylist.php?port=&type=Anonymous&ssl=&country=${country}&latency=&reliability=&sort=reliability&desc=true`;

        try {
            let html = await request({
                url
            });

            let listProxy = [];

            let $ = cheerio.load(html);
            $('.odd').each(function (index, el) {
                let ip = $(this).find('td:nth-child(1)').text().trim();
                let port = $(this).find('td:nth-child(2)').text().trim();
                listProxy.push({ip, port});
            });

            return listProxy.slice(1, limit + 1);
        } catch (err) {
            return [];
        }

    }
};

