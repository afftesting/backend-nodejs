const ValidateHelper = require('./ValidateHelper');

module.exports = {
    findJsAndMetaRedirect(html, url) {
        if (!html) {
            return false
        }

        // console.log(html);

        //appsflyer js redirect ios app
        if (url.indexOf('https://app.appsflyer.com/id') > -1) {
            let matches = /var store_link = "([^"]+)/.exec(html);
            if (matches && ValidateHelper.isURL(matches[1])) {
                return matches[1];
            }
        }

        //find in query string
        let matches = /=(http[%0-9A-Za-z.-]+)/.exec(url);
        if (matches && matches[1]) {
            let link = decodeURIComponent(matches[1]);
            if (ValidateHelper.isURL(link)) {
                return link;
            }
        }

        let listRegexHtml = [
            /location(?:\.href)?(:\s+)?=(?:\s+)?['|"]([-a-zA-Z0-9@:%._\/\+?&~#=]+)/i, //find location.href = 'url'
            /http-equiv="refresh".+content="[0-9];(?:\s+)?URL=['|"]?([-a-zA-Z0-9@:%._\/\+?&~#=]+)/i, //find meta redirect
            /<script[^>]+>[^=]+=\s?['|"]([-a-zA-Z0-9@:%._\/\+?&~#=]+)/i //find first link in first script tag 
        ]

        for (let regex of listRegexHtml) {
            let matches = regex.exec(html);
            if (matches && ValidateHelper.isURL(matches[1])) {
                return matches[1];
            }
        }

        return false
    },
    
    findStatusCode(errorMessage) {
        let matches = /statusCode=(\d{3,})/.exec(errorMessage);
        if (matches && matches[1]) {
            return matches[1];
        }

        return false;
    }
}
