//file: RequestHelper.js

const request = require('request-promise');
const cheerio = require('cheerio');
const _ = require('lodash');
const LogHelper = require('./LogHelper');
const RegexHelper = require('./RegexHelper');
const path = require('path');

module.exports = {

    async getGoogleAppInfo(packageName) {
        try {
            packageName = packageName.replace('gp-', '');
            let response = await request(`https://play.google.com/store/apps/details?id=${packageName}`)
            let $ = cheerio.load(response);

            let logo = $('img[alt="Cover art"]').attr('src');
            let name = $('#main-title').text().replace('- Apps on Google Play', '');

            return {
                logo, name
            }
        } catch (e) {
            return false;
        }
    },

    async getAppleAppInfo(url) {
        try {
            url = url.replace('itms-apps', 'https');
            let response = await request(url);
            let $ = cheerio.load(response);

            let logo = $('.product-hero__media img').attr('src');
            let name = $('title').text().replace('on the App Store', '');

            return {
                logo, name
            }
        } catch (e) {
            return false;
        }
    },

    async followRedirect({proxy, url, user_agent, timeout = 30000}) {
        let redirect_urls = [
            {
                url,
                statusMessage: 'OK',
                statusCode: 200
            }
        ];
        let index = 1;
        let lastTime = new Date().getTime();
        let total_time = 0;
        let last_url = url;

        try {

            let response = await request({
                url,
                headers: {
                    'User-Agent': user_agent
                },
                proxy: `http://${proxy.ip}:${proxy.port}`,
                timeout: timeout,
                resolveWithFullResponse: true,
                followAllRedirects: true,
                followRedirect: (data) => {
                    redirect_urls[index - 1].statusCode = data.statusCode;
                    redirect_urls[index - 1].statusMessage = data.statusMessage;

                    let tempTime = new Date().getTime();
                    redirect_urls[index++ - 1].totalTime = (tempTime - lastTime);
                    total_time += (tempTime - lastTime);
                    lastTime = tempTime;

                    let to_url = data.headers.location;

                    if (to_url.indexOf('..') > -1) {
                        to_url = path.normalize(last_url + to_url);
                    }

                    LogHelper.redirectLog({
                        from_ip: proxy.ip,
                        target_url: last_url,
                        to_url: to_url,
                        status_code: data.statusCode,
                        user_agent: user_agent
                    });

                    last_url = to_url;

                    redirect_urls.push({
                        url: to_url,
                        statusMessage: 'OK',
                        statusCode: 200
                    });
                    return true
                }
            });

            let tempTime = new Date().getTime();
            redirect_urls[index - 1].totalTime = (tempTime - lastTime);
            redirect_urls[index - 1].statusCode = response.statusCode || 200;
            redirect_urls[index - 1].statusMessage = response.statusMessage || 'OK';
            redirect_urls[index - 1].clippedResponse = response.body ? cheerio.load(response.body).text().substr(0, 100) : '';
            total_time += (tempTime - lastTime);
            index++;

            //TODO: parse response get meta redirect
            //TODO: parse response get js redirect

            console.log(response.body);

            let jsMetaRedirectUrl = RegexHelper.findJsAndMetaRedirect(response.body, url);
            console.log(jsMetaRedirectUrl);

            if (jsMetaRedirectUrl) {
                redirect_urls[index - 2].js_meta = true;
                redirect_urls[index - 2].statusMessage = `OK - JS/Meta Redirect`;
                redirect_urls[index - 2].clippedResponse = jsMetaRedirectUrl;
                let flowJSRedirectData = await this.followRedirect({proxy, url: jsMetaRedirectUrl, user_agent, timeout});
                redirect_urls = redirect_urls.concat(flowJSRedirectData.redirect_urls);
                total_time += flowJSRedirectData.total_time;    
            }
            
        } catch (err) {

             console.log(err)
            let protocol = _.get(err, 'response.request.uri.protocol');
            console.log(protocol);
            if (protocol &&
                protocol.indexOf('market') == -1 && 
                protocol.indexOf('itms-apps') == -1
            ) {
                if (err.statusCode) {
                    redirect_urls[index - 1].statusCode = err.statusCode;
                }

                if (err.response) {
                    redirect_urls[index - 1].statusMessage = err.response.statusMessage;
                } else {
                    redirect_urls[index - 1].statusMessage = 403;
                }
            } else if (err.message.indexOf('ESOCKETTIMEDOUT') > -1) {
                 redirect_urls[index - 1].statusCode = 408;
                 redirect_urls[index - 1].statusMessage = 'Request Timeout';
	    } else if (RegexHelper.findStatusCode(err.message)) {
                 redirect_urls[index - 1].statusCode = RegexHelper.findStatusCode(err.message);
                 redirect_urls[index - 1].statusMessage = 'Error';
            }

            let tempTime = new Date().getTime();
            redirect_urls[index++ - 1].totalTime = (tempTime - lastTime);
            total_time += (tempTime - lastTime);
        }

        return {
            redirect_urls,
            total_time
        };
    }
};
