module.exports = {
    isURL(string) {
        try {
            new URL(string.trim());
            return true;

        } catch (e) {
            return false;
        }
    },
}