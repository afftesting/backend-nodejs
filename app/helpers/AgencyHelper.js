const Agency = require('../models/Agency');

module.exports = {
    async findAndUpdate({root_domain, url}) {

        try {
            let agency = await Agency.findOne({
                root_domain
            }).exec();

            if (!agency) {
                return false;
            }

            agency.all_domain.push(url);
            agency.save();

            return {
                name: agency.name,
                root_domain: agency.root_domain,
                logo: agency.logo
            }
        } catch (e) {
            return false;
        }
    },

    async addAgency({name, logo, root_domain, all_domain}) {
        return await (new Agency({
            name, logo, root_domain, all_domain: all_domain || []
        })).save();
    }
}