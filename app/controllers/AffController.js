//file: AffController.js
const DeviceHelper = require('../helpers/DeviceHelper');
const ProxyHelper = require('../helpers/ProxyHelper');
const RequestHelper = require('../helpers/RequestHelper');
const ValidateHelper = require('../helpers/ValidateHelper');

module.exports = {
    async testing(req, res) {
        const {
            url, country, device, os_version
        } = req.body;

        if (!ValidateHelper.isURL(url)) {
            res.status(400).json({
                error: 'Invalid URL'
            });
            return;
        }

        const user_agent = DeviceHelper.getUserAgent({device, os_version});
        const listProxy = await ProxyHelper.getProxy({country, limit: 1});

        if (listProxy.length === 0) {
            return res.json({
                error: 'Cannot test with this country'
            });
        }
         
        let listActions = listProxy.map(proxy => RequestHelper.followRedirect({proxy, url, user_agent}));

        try {
            let data = await Promise.all(listActions);

            res.json({
                data
            });
        } catch (err) {
            console.log(err);
            
            res.json({
                error: 'Error while testing link'
            })
        }
    }
};