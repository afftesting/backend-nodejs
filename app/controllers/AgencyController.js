
const AgencyHelper = require('../helpers/AgencyHelper');
const RequestHelper = require('../helpers/RequestHelper');
const ValidateHelper = require('../helpers/ValidateHelper');
module.exports = {
    async getAgencyInfo(req, res) {
        const {url} = req.query;

        if (!ValidateHelper.isURL(url)) {
            return res.status(400).json({
                error: 'Invalid domain'
            })
        }

        let hostname = new URL(url).hostname;
        let arrHostname = hostname.split('.').reverse();
        let root_domain = arrHostname.slice(0, 2).reverse().join('.');

        if (url.indexOf('market://details') > -1) {
            let matches = /market:\/\/details\?id=([a-zA-Z0-9.]+)/.exec(url);

            if (matches && matches[1]) {
                root_domain = 'gp-' + matches[1];
            }
        } else if (url.indexOf('itms-apps://') > -1) {
            let matches = /app\/([id0-9]+)/.exec(url);

            if (matches && matches[1]) {
                root_domain = 'ios-' + matches[1];
            }
        }

        let agency = await AgencyHelper.findAndUpdate({root_domain, url});

        if (agency) {
            return res.json({
                success: true,
                data: agency
            });
        }

        let appInfo = undefined;

        if (url.indexOf('market://details') > -1) {
            appInfo = await RequestHelper.getGoogleAppInfo(root_domain);
        } else if (url.indexOf('itms-apps://') > -1) {
            appInfo = await RequestHelper.getAppleAppInfo(url);
        }

        console.log(appInfo);

        if (appInfo) {
            agency = {
                name: appInfo.name,
                logo: appInfo.logo,
                root_domain: root_domain,
                all_domain: [url]
            };

            AgencyHelper.addAgency(agency);

            agency.all_domain = undefined;

            return res.json({
                success: true,
                data: agency
            });
        }

        return res.json({
            success: false,
            error: 'Agency not found'
        });
    },

    async storeAgency(req, res) {
        let body = req.body;

        if (body.author != 'dangdungcntt@gmail.com') {
            return res.status(404).json({
                error: 'Not found'
            });
        }
        
        res.json({
            success: AgencyHelper.addAgency(body) 
        })
    }
}