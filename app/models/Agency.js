//file: RedirectLog.js

let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let AgencySchema = new Schema(
    {
        name: {type: String},
        logo: {type: String},
        root_domain: {type: String},
        all_domain: {type: Array, default: []}
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

//Export model
module.exports = mongoose.model('Agency', AgencySchema);