//file: RedirectLog.js

let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let RedirectLogSchema = new Schema(
    {
        from_ip: {type: String},
        target_url: {type: String},
        to_url: {type: String},
        status_code: {type: String},
        user_agent: {type: String}
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

//Export model
module.exports = mongoose.model('RedirectLog', RedirectLogSchema);